import javax.swing.JButton;
import javax.swing.JFrame;

public class FirstSwingExample {
    public static void main(String[] args) {
        JFrame f = new JFrame();
        f.setTitle("First JFrame");
        JButton button = new JButton("Click");
        button.setBounds(130,100,100,40);
        f.add(button);
        f.setLayout(null);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
        f.setSize(400,500);
    }   
}
