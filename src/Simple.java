import javax.swing.JButton;
import javax.swing.JFrame;
class MyApp {
    JFrame f;
    JButton button;
    public MyApp() {
        f = new JFrame();
        f.setTitle("First JFrame");
        button = new JButton("Click");
        button.setBounds(130,100,100,40);
        f.add(button);
        f.setLayout(null);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
        f.setSize(400,500);
    }
}
public class Simple {
    public static void main(String[] args) {
        MyApp app = new MyApp();
    }
}
